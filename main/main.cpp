#include <Arduino.h>
#include <HardwareSerial.h>
#include <time.h>
#include <sys/time.h>
#include <driver/adc.h>
 #include "soc/soc.h"
 #include "soc/rtc_cntl_reg.h"
#include "driver/rtc_io.h"
#include "pins.h"
#include "sd_fonc.h"
#include "server.h"
#include "can.h"
#include "config.h"

carteSd sd1(5, VSPI);
carteSd sd2(15, HSPI);
HardwareSerial can(1);
ADS122U04 conv(35);

etat_t etatCourant;
RTC_DATA_ATTR static int bootCount = 0;
RTC_DATA_ATTR int fileSd1, fileSd2;
RTC_DATA_ATTR uint8_t sleepTime = 20;
RTC_DATA_ATTR float sauvData[NBDATASD-1];
RTC_DATA_ATTR time_t times[NBDATASD-1];

time_t debut, before, now;
extern time_t rtc;

void deepSleep(uint8_t sec);
void lightSleep(uint8_t sec);
void ledFlash(void);
void initCan(void);
float readVoltBat(void);

float data = 0;

bool stopTimerMain = false;
extern volatile bool stopTimer;
extern portMUX_TYPE timerMux;

extern bool streaming;
bool serverON = false;

void setup()
{
        WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0);
        debut = time(&rtc);
        ++bootCount;
        esp_sleep_wakeup_cause_t restart = esp_sleep_get_wakeup_cause();
        pc.begin(115200);
        can.begin(9600, SERIAL_8N1, 4, 2);
        pinMode(led, OUTPUT);
        pinMode(regSd, OUTPUT);
        pinMode(regCan, OUTPUT);
        pinMode(comWind, OUTPUT);
        pinMode(bouton, INPUT_PULLUP);

        if(restart == ESP_SLEEP_WAKEUP_TIMER) { //Restart a cause du timer
                rtc_gpio_hold_dis(GPIO_NUM_26);
                rtc_gpio_hold_dis(GPIO_NUM_27);
                sd1.numFile = fileSd1;
                sd2.numFile = fileSd2;
                //pc.println(hallRead());
                if(hallRead()>SEUILHALL) {
                        digitalWrite(regSd, HIGH);
                        delay(50);
                        sd2.streamCSV();
                        digitalWrite(regSd, LOW);
                        monitor_Wifi();
                        serverON = true;
                }
                etatCourant = LECTURE;
        }
        else {
                digitalWrite(regSd, HIGH);
                delay(50);
                fileSd1 = sd1.init("time,temperature");
                fileSd2 = sd2.init("time,temperature");
                digitalWrite(regSd, LOW);
                etatCourant = INIT;
        }
}


void loop()
{
        float temp;
        now = time(&rtc);
        switch(etatCourant) {
        case INIT:
                wifi_Init();
                alarm();
                etatCourant = INIT_WIFI;
                break;

        case INIT_WIFI:
                portENTER_CRITICAL_ISR(&timerMux);
                stopTimerMain = stopTimer;
                portEXIT_CRITICAL_ISR(&timerMux);
                if(!digitalRead(bouton)) {
                        pc.println("appui Off");
                        while(!digitalRead(bouton));
                        stopWifi();
                        etatCourant = LECTURE;
                }
                if(stopTimerMain) {
                        stopWifi();
                        stopTimerMain = false;
                        portENTER_CRITICAL_ISR(&timerMux);
                        stopTimer = false;
                        portEXIT_CRITICAL_ISR(&timerMux);
                        etatCourant = LECTURE;
                }
                break;

        case LECTURE:
                digitalWrite(regCan, HIGH);
                delay(50);
                conv.reset();
                delay(50);
                conv.begin();

                delay(70);
                conv.startSync();
                delay(70);
                temp = conv.manualReadVolt();

                conv.startSync();
                delay(70);
                temp = conv.manualReadVolt();
                conv.powerDown();
                data = -(3.0*(R0*R0)+R1*temp*(R0+R1))/(0.00385*R0*(3.0*(R0-R1)+R1*temp)*16.0); //Wheatstone et PGA
                data = KA * data + KB; //Correction erreur conditionement
                pc.printf("%f -- %f\n", temp, data);
                digitalWrite(regCan, LOW);
                etatCourant = ECRITURE;
                break;

        case ECRITURE:
                if(serverON)
                        sendDataWs(data, now, "Temperature", "°C"); //Envoi websocket pour page Capteurs
                if(bootCount == NBDATASD) {
                        digitalWrite(regSd, HIGH);
                        delay(50);
                        if(NBDATASD != 1) {
                                for(int i = 0; i < NBDATASD-1; i++) {
                                        sd1.logger(times[i], 1, sauvData[i]);
                                }
                        }
                        sd1.logger(now, 1, data);
                        delay(20);
                        if(NBDATASD != 1) {
                                for(int i = 0; i < NBDATASD-1; i++) {
                                        sd2.logger(times[i], 1, sauvData[i]);
                                }
                        }
                        sd2.logger(now, 1, data);
                        //if(!serverON)
                        digitalWrite(regSd, LOW);
                        bootCount = 0;
                }
                else {
                        sauvData[bootCount-1] = data;
                        times[bootCount-1] = now;
                }
                etatCourant = DODO;
                break;

        case DODO:
                /*if(readVoltBat()<2.6) {
                  ...........
                }*/
                if(serverON) {
                        time_t diff = time(&rtc) - debut;
                        delay((sleepTime-diff)*1000);
                        debut = time(&rtc);
                        if(hallRead()>SEUILHALL) {
                                stopWifi();
                                serverON = false;
                        }
                        etatCourant = LECTURE;
                        ++bootCount;
                }
                else
                        deepSleep(sleepTime);
                break;

        default:
                pc.println("Erreur Machine a etat !");
        }
        before = now;
        //pc.println(etatCourant);
        delay(10);
}

void deepSleep(uint8_t sec)
{
        digitalWrite(regSd, LOW);
        digitalWrite(regCan, LOW);
        digitalWrite(comWind, LOW);
        rtc_gpio_hold_en(GPIO_NUM_26);
        rtc_gpio_hold_en(GPIO_NUM_27);
        rtc_gpio_hold_en(GPIO_NUM_21);
        time_t diff = time(&rtc) - debut;
        esp_sleep_enable_ext0_wakeup(GPIO_NUM_22, 0);
        esp_sleep_enable_timer_wakeup((sec-diff)*1000000);
        esp_deep_sleep_start();
}

void lightSleep(uint8_t sec)
{
        esp_sleep_enable_timer_wakeup(sec*1000000);
        esp_light_sleep_start();
}

void ledFlash(void)
{
        static uint8_t periode = 0;
        if(before != now) {
                if(periode == LEDRATE-1) {
                        digitalWrite(led, HIGH);
                        delay(20);
                        digitalWrite(led, LOW);
                        periode = 0;
                }
                else
                        periode++;
        }
}

float readVoltBat(void)
{
        adc1_config_width(ADC_WIDTH_BIT_10);
        adc1_config_channel_atten(ADC1_CHANNEL_6,ADC_ATTEN_DB_11);
        int value = adc1_get_raw(ADC1_CHANNEL_6);
        float voltRaw = 2.0*3.6/1023.0*(float)value;
        return (kAV*voltRaw+kBV);
}
