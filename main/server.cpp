#include <Arduino.h>
#include <WiFi.h>
#include <esp_wifi.h>
#include <time.h>
#include <sys/time.h>
#include "FS.h"
#include "SD.h"
#include "SPI.h"
#include "SPIFFS.h"
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include "AsyncJson.h"
#include "ArduinoJson.h"
#include "server.h"
#include "sd_fonc.h"
#include "pins.h"

#include <esp_pm.h>
#include <esp_wifi.h>
#include <esp_wifi_types.h>

AsyncWebServer server(80);
AsyncWebSocket ws("/ws");

const char *ssid = "Logger";
const char *password = "password";

time_t rtc;
extern time_t debut;
extern etat_t etatCourant;

hw_timer_t * timer = NULL;
volatile bool stopTimer = false;
portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED;

extern RTC_DATA_ATTR uint8_t sleepTime;

extern carteSd sd1;
SPIClass SD_SPI(VSPI);

bool streaming = false;
extern bool serverON;

void IRAM_ATTR onTimer()
{
        portENTER_CRITICAL_ISR(&timerMux);
        stopTimer = true;
        portEXIT_CRITICAL_ISR(&timerMux);
}

void wifi_Init(void)
{
        digitalWrite(led, HIGH);
        WiFi.mode(WIFI_AP);
        WiFi.softAP(ssid, password);
        if(!SPIFFS.begin()) {
                DEBUG_PRINTLN("Error SPIFFS !");
                return;
        }

        server.serveStatic("/js/", SPIFFS, "/js/");
        server.serveStatic("/css/", SPIFFS, "/css/");
        if(SPIFFS.exists("/indexinit.html")) {
                server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
                        stopAlarm();
                        int paramsNr = request->params();
                        DEBUG_PRINTLN(paramsNr);
                        for(int i=0; i<paramsNr; i++) {
                                AsyncWebParameter* p = request->getParam(i);
                                if(p->name()=="timestamp") {
                                        rtc = atoi(p->value().c_str());
                                        DEBUG_PRINTLN(rtc);
                                        timeval tv = { rtc, 0 };
                                        timezone tz = { TZ_MN + DST_MN, 0 };
                                        settimeofday(&tv, &tz);
                                        //stopWifi();
                                }
                                if(p->name()=="rate") {
                                        sleepTime = (uint8_t)atoi(p->value().c_str());
                                        stopWifi();
                                }
                        }
                        request->send(SPIFFS, "/indexinit.html", "text/html");
                        //request->send(200, "text/html", HTML);
                });
        }
        server.begin();
}

void monitor_Wifi(void)
{
        digitalWrite(led, HIGH);
        WiFi.mode(WIFI_AP);
        WiFi.softAP(ssid, password);
        server.addHandler(&ws);
        if(!SPIFFS.begin()) {
                DEBUG_PRINTLN("Error SPIFFS !");
                return;
        }
        server.serveStatic("/js/", SPIFFS, "/js/");
        server.serveStatic("/css/", SPIFFS, "/css/");
        if(SPIFFS.exists("/index.html")) {
                server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
                        int paramsNr = request->params();
                        DEBUG_PRINTLN(paramsNr);
                        for(int i=0; i<paramsNr; i++) {
                                AsyncWebParameter* p = request->getParam(i);
                                if(p->name()=="timestamp") {
                                        rtc = atoi(p->value().c_str());
                                        DEBUG_PRINTLN(rtc);
                                        timeval tv = { rtc, 0 };
                                        timezone tz = { TZ_MN + DST_MN, 0 };
                                        settimeofday(&tv, &tz);
                                }
                                if(p->name()=="rate") {
                                        sleepTime = (uint8_t)atoi(p->value().c_str());
                                }
                                if(p->name()=="shutdown") {
                                        serverON = false;
                                        stopWifi();
                                }
                        }
                        request->send(SPIFFS, "/index.html", "text/html");
                });
        }
        if(SPIFFS.exists("/graph.html")) {
                server.on("/graph", HTTP_GET, [](AsyncWebServerRequest *request){
                        request->send(SPIFFS, "/graph.html", "text/html");
                });
        }
        if(SPIFFS.exists("/data.html")) {
                server.on("/data", HTTP_GET, [](AsyncWebServerRequest *request){
                        request->send(SPIFFS, "/data.html", "text/html");
                });
        }
        if(SPIFFS.exists("/logger.csv")) {
                server.on("/logger.csv", HTTP_ANY, [](AsyncWebServerRequest *request){
                        request->send(SPIFFS, "/logger.csv", "text/csv");
                });
        }

        server.begin();
}

void sendDataWs(float data1, unsigned long data2, char const *titre, char const *unit)
{
        if (!ws.enabled())
          ws.enable(true);
        DynamicJsonBuffer jsonBuffer;
        JsonObject& root = jsonBuffer.createObject();
        root[titre] = data1;
        root["timestamp"] = data2;
        root["unit"] = unit;
        size_t len = root.measureLength();
        AsyncWebSocketMessageBuffer * buffer = ws.makeBuffer(len);
        if (buffer) {
                root.prettyPrintTo(Serial);
                root.printTo((char *)buffer->get(), len + 1);
                ws.textAll(buffer);
        }
        jsonBuffer.clear();
        ws._cleanBuffers();
}

void alarm(void)
{
        timer = timerBegin(0, 80, true);
        timerAttachInterrupt(timer, &onTimer, true);
        timerAlarmWrite(timer, 60*1000000, true);
        timerAlarmEnable(timer);
}

void stopAlarm(void)
{
        if(timer) {
                timerEnd(timer);
                timer = NULL;
        }
}

void stopWifi(void)
{
        stopAlarm();
        digitalWrite(led, LOW);
        digitalWrite(regSd, LOW);
        esp_wifi_stop();
        etatCourant = LECTURE;
        debut = time(&rtc);
}
