#ifndef CONFIG_H
#define CONFIG_H

#define SLEEP_TIME 10 //secondes

#define NBDATASD 1 //Enregistrement sur carte SD toutes les X acquisitions

#define LEDRATE 3 //Periode en sec du flash lumineux

#define ADC 0 //ADS122U04 0 = inactif  1 = actif

#define SEUILHALL 23

#define R0 100.0
#define R1 3300.0

#define KA 0.905f //SONDE AVEC FILTRE
#define KB -0.871f

#define kAV 1.238f
#define kBV -0.154f

#endif
