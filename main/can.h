#ifndef CAN_H
#define CAN_H

#define DEBUG 1

#ifdef DEBUG
 #define DEBUG_PRINT(x) Serial.print(x)
 #define DEBUG_PRINTX(x,y) Serial.print(x,y)
 #define DEBUG_PRINTLN(x) Serial.println(x)
 #define DEBUG_PRINTLNX(x,y) Serial.println(x,y)
#else
 #define DEBUG_PRINT(x)
 #define DEBUG_PRINTX(x,y)
 #define DEBUG_PRINTLN(x)
 #define DEBUG_PRINTLNX(x,y)
#endif

#define SYNC 0x55
#define RESET 0x07
#define START 0x08
#define POWERDOWN 0x02
#define RDATA 0x10
#define RREG  0x20
#define WREG 0x40

#define CONFIG_REG0_ADDRESS 0x00
#define CONFIG_REG1_ADDRESS 0x01
#define CONFIG_REG2_ADDRESS 0x02
#define CONFIG_REG3_ADDRESS 0x03
#define CONFIG_REG4_ADDRESS 0x04

#define REG_CONFIG_DR_20SPS 0x00
#define REG_CONFIG_DR_45SPS 0x20
#define REG_CONFIG_DR_90SPS 0x40
#define REG_CONFIG_DR_175SPS 0x60
#define REG_CONFIG_DR_330SPS 0x80
#define REG_CONFIG_DR_600SPS 0xA0
#define REG_CONFIG_DR_1000SPS 0xC0
#define REG_CONFIG_DR_MASK 0xE0

#define DR_20SPS 20
#define DR_45SPS 45
#define DR_90SPS 90
#define DR_175SPS 175
#define DR_330SPS 330
#define DR_600SPS 600
#define DR_1000SPS 1000

#define REG_CONFIG_PGA_GAIN_1 0x00
#define REG_CONFIG_PGA_GAIN_2 0x02
#define REG_CONFIG_PGA_GAIN_4 0x04
#define REG_CONFIG_PGA_GAIN_8 0x06
#define REG_CONFIG_PGA_GAIN_16 0x08
#define REG_CONFIG_PGA_GAIN_32 0x0A
#define REG_CONFIG_PGA_GAIN_64 0x0C
#define REG_CONFIG_PGA_GAIN_128 0x0E
#define REG_CONFIG_PGA_GAIN_MASK 0x0E

#define PGA_GAIN_1 1
#define PGA_GAIN_2 2
#define PGA_GAIN_4 4
#define PGA_GAIN_8 8
#define PGA_GAIN_16 16
#define PGA_GAIN_32 32
#define PGA_GAIN_64 64
#define PGA_GAIN_128 128

#define AIN0_AIN1_CONFIG 0x00
#define AIN0_AIN2_CONFIG 0x10
#define AIN0_AIN3_CONFIG 0x20
#define AIN1_AIN0_CONFIG 0x30
#define AIN1_AIN2_CONFIG 0x40
#define AIN1_AIN3_CONFIG 0x50
#define AIN2_AIN3_CONFIG 0x60
#define AIN3_AIN2_CONFIG 0x70
#define AIN0_AVSS_CONFIG 0x80
#define AIN1_AVSS_CONFIG 0x90
#define AIN2_AVSS_CONFIG 0xA0
#define AIN3_AVSS_CONFIG 0xB0
#define VREFP_VREFN_4 0xC0
#define AVDD_AVSS_4 0xD0
#define AINP_AINN_AVDD_AVSS_2 0xE0
#define INPUT_MULTIPLEXER_MASK 0xF0

#define GPIO0 0
#define GPIO1 1
#define GPIO2 2

#define _UN(bit) (1<<(bit))

class ADS122U04
{
public:
  uint8_t NewDataAvailable;

  ADS122U04();
  ADS122U04(uint8_t m_drdy);
  void begin(void);
  void reset(void);
  void startSync(void);
  void powerDown(void);
  void PGA_ON(void);
  void PGA_OFF(void);
  void temperatureSensorON(void);
  void temperatureSensorOFF(void);
  void continuousConversionModeON(void);
  void singleShotModeON(void);
  void normalModeON(void);
  void turboModeON(void);
  void gpio2ControlON(void);
  void drdyControlON(void);
  void setGpio(int gpio, int io);
  void setInputMultiplexer(int setting);
  void setDataRate(int datarate);
  void setPgaGain(int pgagain);
  uint8_t * getConfigReg(void);
  uint32_t manualRead(void);
  float manualReadVolt(void);
  uint32_t autoRead(void);
  float autoReadVolt(void);
  float readTemp(void);

private:
  uint8_t Config_Reg0;
  uint8_t Config_Reg1;
  uint8_t Config_Reg2;
  uint8_t Config_Reg3;
  uint8_t Config_Reg4;
  uint8_t drdy;

  void can_write(uint8_t valeur);
  void writeReg(uint8_t registre, uint8_t valeur);
  uint8_t readReg(uint8_t registre);

};

 #endif
