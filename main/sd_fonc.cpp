#include <Arduino.h>
#include <stdio.h>
#include <stdarg.h>
#include <ArduinoJson.h>
#include "FS.h"
#include "SD.h"
#include "SPI.h"
#include "SPIFFS.h"
#include "sd_fonc.h"
#include "config.h"

const char * entete = "#LOGGER_VENT";

carteSd::carteSd(uint8_t m_ssPin, SPIClass spi) : ssPin(m_ssPin), spi_bus(spi) {
}

int carteSd::init(const char * title)
{
        if (!SD.begin(ssPin, spi_bus)) {
                DEBUG_PRINTLN("Carte non presente -- INIT");
                return 0;
        }
        DEBUG_PRINTLN("Carte Initialise");
        int n = 1;
        sprintf(filename, "/logger%02d.csv", n);
        while(SD.exists(filename)) {
                n++;
                sprintf(filename, "/logger%02d.csv", n);
        }
        File dataFile = SD.open(filename, FILE_WRITE);
        if(dataFile) {
                dataFile.println(entete);
                dataFile.println(title);
                dataFile.close();
                DEBUG_PRINT("Ecriture reussie sur : ");
                DEBUG_PRINTLN(filename);
                SD.end();
        }
        else {
                DEBUG_PRINT("Ecriture impossible sur : ");
                DEBUG_PRINTLN(filename);
        }
        numFile = n;
        return n;
}

void carteSd::logger(unsigned long timestamp, int n_args, ...)
{
        va_list ap;
        va_start(ap, n_args);

        if (!SD.begin(ssPin, spi_bus)) {
                DEBUG_PRINTLN("Carte non presente -- LOGGER");
                return;
        }

        Serial.println(ssPin);

        sprintf(filename, "/logger%02d.csv", numFile);

        File dataFile = SD.open(filename, FILE_APPEND);
        if(dataFile) {
                char buf[50];
                double in[n_args];
                for(int i=0; i<n_args; i++) {
                        in[i] = va_arg(ap, double);
                }
                switch(n_args) {
                case 1:
                        sprintf(buf, "%lu,%.3f", timestamp, in[0]);
                        break;

                case 2:
                        sprintf(buf, "%lu,%.3f,%.3f", timestamp, in[0], in[1]);
                        break;

                case 3:
                        sprintf(buf, "%lu,%.3f,%.3f,%.3f", timestamp, in[0], in[1], in[2]);
                        break;

                case 4:
                        sprintf(buf, "%lu,%.3f,%.3f,%.3f,%.3f", timestamp, in[0], in[1], in[2], in[3]);
                        break;
                }
                dataFile.println(buf);
                dataFile.close();
                SD.end();
        }
        else {
                DEBUG_PRINT("Ecriture impossible sur : ");
                DEBUG_PRINTLN(filename);
                SD.end();
        }
        va_end(ap);
        nbData = n_args+1;
}

char * carteSd::lastFile(void)
{
        sprintf(filenameFind, "/logger%02d.csv", numFile);
        return filenameFind;
}

void carteSd::streamCSV(void)
{
        if (!SD.begin(ssPin, spi_bus)) {
                DEBUG_PRINTLN("Carte non presente -- LOGGER");
                return;
        }
        File dataFile = SD.open(lastFile());

        unsigned long fileSize = dataFile.size();

        while(dataFile.read()!='\n'); //Saut de 1 ligne

        char title[30];
        char inData;

        uint8_t index = 0;
        do {
                title[index+1] = '\0';
                inData = dataFile.read();
                title[index] = inData; //Récupération de la ligne de titre
                index++;
        } while(inData!='\n');

        char oneline[30];
        inData = 0;
        index = 0;
        do {
                oneline[index+1] = '\0';
                inData = dataFile.read();
                oneline[index] = inData; //Récupération de la premiere ligne de données pour évaluer sa taille
                index++;
        } while(inData!='\n');

        size_t bytesPerLine = strlen(oneline);
        unsigned long nbLines = (fileSize/(unsigned long)bytesPerLine)-2;

        if(!SPIFFS.begin()) {
                DEBUG_PRINTLN("Error SPIFFS !");
                return;
        }
        File streamFile = SPIFFS.open("/logger.csv", FILE_WRITE);

        if(nbLines <= 5000) {
                dataFile.seek(0);
                while(dataFile.available()) {
                        streamFile.write(dataFile.read());
                }
        }
        else {
                streamFile.print(title);
                dataFile.seek(fileSize-(5000*bytesPerLine));
                while(dataFile.read()!='\n');
                while(dataFile.available()) {
                        streamFile.write(dataFile.read());
                }
        }
        dataFile.close();
        streamFile.close();
        SD.end();
}
