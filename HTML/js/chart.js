var data = [
  []
];
var time = [];
var titre = [];
var nbData;

Papa.parse("logger.csv", {
  delimiter: ',',
  header: true,
  comments: true,
  download: true,
  skipEmptyLines: true,
  worker: false,
  complete: function(results) {
    var length = results.data.length;
    var title = Object.keys(results.data[0]);
    var titleLength = title.length;
    nbData = titleLength - 1;
    for (var i = 0; i < (titleLength - 1); i++) {
      titre[i] = title[i + 1];
      titre[i] = titre[i].charAt(0).toUpperCase() + titre[i].slice(1);
    }
    for (var j = 0; j < (titleLength - 1); j++) {
      data[j] = [];
    }
    for (var i = 0; i < length; i++) {
      var date = new Date(results.data[i].time * 1000);
      var year = date.getFullYear();
      var month = date.getMonth() + 1;
      var day = date.getDate();
      var hour = date.getHours();
      var min = date.getMinutes();
      var sec = date.getSeconds();
      month = (month < 10 ? "0" : "") + month;
      day = (day < 10 ? "0" : "") + day;
      hour = (hour < 10 ? "0" : "") + hour;
      min = (min < 10 ? "0" : "") + min;
      sec = (sec < 10 ? "0" : "") + sec;
      var str = day + "/" + month + "/" + year + " " + hour + ":" + min + ":" + sec;
      time[i] = str;
      for (var j = 0; j < (titleLength - 1); j++) {
        var inside = "results.data["+i+"]."+title[j+1];
        data[j][i] = eval(inside);
      }
    }
    var alink = [];
    for(var i=0; i<nbData; i++) {
      alink[i] = document.createElement("button");
      alink[i].id = "btnchart"+i;
      alink[i].className = "btn btn-danger";
      alink[i].type = "button";
      alink[i].setAttribute("onclick", "updateChart("+i+")");
      document.getElementById("chartbutton").appendChild(alink[i]);
      document.getElementById("btnchart"+i).innerHTML = titre[i];
    }
    chart.data.datasets[0].data = data[0];
    chart.options.title.text = titre[0];
    chart.update();
  }
});

Chart.defaults.global.legend.display = false;

var config = {
  type: 'line',
  data: {
    labels: time,
    datasets: [{
      data: data[0],
      borderColor: "#cc0000",
      fill: false
    }]
  },
  options: {
    responsive: true,
    title: {
      display: true,
      text: titre[0]
    },
    pan: {
      enabled: true,
      sensitivity: 0.1,
      mode: 'xy'
    },
    zoom: {
      enabled: true,
      //drag: true,
      sensitivity: 1,
      mode: 'x'
    }
  }
};

var ctx = document.getElementById("chart").getContext("2d");
var chart = new Chart(ctx, config);

function resetZoom() {
  chart.resetZoom();
}

function updateChart(button) {
  chart.data.datasets[0].data = data[button];
  chart.options.title.text = titre[button];
  chart.update();
}
