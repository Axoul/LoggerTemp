var h;
var m;
var s;
var year;
var month;
var day;

function startTime() {
  var today = new Date();
  year = today.getFullYear();
  month = today.getMonth() + 1;
  day = today.getDate();
  h = today.getHours();
  m = today.getMinutes();
  s = today.getSeconds();
  m = checkTime(m);
  s = checkTime(s);
  month = checkTime(month);
  day = checkTime(day);
  document.getElementById('txt').innerHTML = "Il est " + h + ":" + m + ":" + s + " et nous sommes le " + day + "/" + month + "/" + year;
  var t = setTimeout(startTime, 500);
}

function checkTime(i) {
  if (i < 10) {
    i = "0" + i
  };
  return i;
}

function sendData() {
  var unix = Math.round(+new Date() / 1000);
  window.location.href = "?timestamp=" + unix + "&rate=" + rate.value;
}

function shutdown() {
  window.location.href = "?shutdown=true";
}

var rate = document.getElementById("rate");
