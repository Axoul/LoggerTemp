var dataSocket = new WebSocket("ws://192.168.4.1/ws");

var title = [];
var time;
var data;
var unit;

dataSocket.onmessage = function(event) {
  var msg = JSON.parse(event.data);
  unit = msg.unit;
  var timein = new Date(msg.timestamp * 1000);
  var timeStr = timein.toLocaleTimeString('fr-FR');
  var options = {
    weekday: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric'
  };
  var dateStr = timein.toLocaleDateString('fr-FR', options);
  time = dateStr + " à " + timeStr;
  title[0] = Object.keys(msg)[0];
  data = eval("msg." + title[0]);
  title[0] = capitalizeFirstLetter(title[0]);
  title[1] = "Date et Heure";
  updateTable();
}

function updateTable() {
  document.getElementById("title1").innerHTML = title[0];
  document.getElementById("title2").innerHTML = title[1];
  document.getElementById("data1").innerHTML = data + " " + unit;
  document.getElementById("data2").innerHTML = time;
}

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}
